<p align="center"><img src="http://i.imgur.com/F19ds2q.jpg"/></p>

<h1>Slim_gapps</h1>

<p><b>Dependencies</b>: <i>java-environment, apache-ant</i><p>
<p><a href="http://ant.apache.org/manual/index.html">here</a>'s more info on apache ant.</p>

<h2>How to use?</h2>

<p>the build is initiated by running <b>"ant"</b> from the root directory on command line. if you don't have ant installed, you can get it by running "sudo apt-get install ant" from the command line.</p>

<p>if you take a look inside the folder "optional" in the root directory of that repo, you can see the scripts that is responsible for generating the gapps packages. they all are end up postfix "-build.xml". by modifying this scripts, you can add/remove apps to suit your needs.
</p>

<h3>Gapps packages can be downloaded here:</h3>
<p><a href="http://104.236.22.120/index.php?device=gapps/lollipop">mirror1</a></p>
<p><a href="http://209.95.39.164/Gapps/Lollipop/">mirror2</a></p>

<p><a href="http://forum.xda-developers.com/slimroms/general/gapps-official-slim-gapps-trds-slimkat-t2792842">General Discussion</a></p>