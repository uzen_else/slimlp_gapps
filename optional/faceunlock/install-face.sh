#!/sbin/sh

if [ -e /system/etc/permissions/android.hardware.camera.front.xml ]; then
  echo "Installing face detection support"
  cp -a /tmp/face/* /system/
  chmod 755 /system/addon.d/71-gapps-faceunlock.sh
fi
rm -rf /tmp/face
